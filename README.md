# BuildSim_Nordic_2022

Technical data, models and results used in the article "Air-, ground- and dual-source heat pumps: a comparison between
energy-efficient systems for a Swedish dwelling." The economic part is not included.

## Installation

1. Download and install [Julia](https://julialang.org/downloads/)
2. Open Julia
3. Enter the Julia package manager by typing `]`
4. Copy the following line `add https://gitlab.com/mlfasci/HeatPump.git `
5. Copy the following line `add https://gitlab.com/mlfasci/buildsim_nordic_2022.git`

The installation is completed and you are ready to run the examples from the article or simulate your own scenarios.

If you have any problem or question, contact mlfasci@kth.se


