using buildsim_nordic_2022
# files used in all the scenarios
    Tair_filename = "T_Stockholm.xlsx"
    Tcondin_SH_filename = "House_Output_tr"
    loadhp_SH_filename = "House_Output_Load"
    loadhp_DHW_filename = "DHW_load.jld"

# Simulation
    results = simulate_air_hp(Tair_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename)

# Save the results
# save("results_air.jld", "results", results)
using JLD
results_air = load(joinpath(@__DIR__,"examples_results\\results_air.jld"))
