using buildsim_nordic_2022
# files used in all the scenarios
    Tcondin_SH_filename = "House_Output_tr"
    loadhp_SH_filename = "House_Output_Load"
    loadhp_DHW_filename = "DHW_load.jld"

    n = 25; # years simulated

# Example 1 : no neighbours, H = 50 m
    response_filename = "FLS_25y_granite_r0575_H50.jld"
    H = 50.;
    results = simulate_ground_hp(response_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n )
    # Save the results
    # save("results_ground_H50dInf.jld", "results", results)

# Example 2 : no neighbours, H = 100 m
    response_filename = "FLS_50y_granite_r0575_H100.jld"
    H = 100.;
    results = simulate_ground_hp(response_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n )
    # Save the results
    save("results_ground_H100dInf.jld", "results", results)

# Example 3 : with neighbours at 15 m , H = 50 m
    response_filename = "FLS_25y_granite_r0575_H50.jld"
    response_neighbourhood_filename = "neighbours_influence_25y_granite_d15_r165_H50.jld"
    H = 50.;
    results = simulate_ground_hp(response_filename, response_neighbourhood_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n)
    # Save the results
    save("results_ground_H50d15.jld", "results", results)

# Example 4 : with neighbours at 20 m, H = 50 m
    response_filename = "FLS_25y_granite_r0575_H50.jld"
    response_neighbourhood_filename = "neighbours_influence_25y_granite_d20_r165_H50.jld"
    H = 50.;
    results = simulate_ground_hp(response_filename,  response_neighbourhood_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n )
    # Save the results
    save("results_ground_H50d20.jld", "results", results)

# Example 5 : with neighbours at 15 m , H = 100 m
    response_filename = "FLS_50y_granite_r0575_H100.jld"
    response_neighbourhood_filename = "neighbours_influence_25y_granite_d15_r165_H100.jld"
    H = 100.;
    results = simulate_ground_hp(response_filename, response_neighbourhood_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n)
    # Save the results
    save("results_ground_H100d15.jld", "results", results)

# Example 6 : with neighbours at 20 m, H = 100 m
    response_filename = "FLS_50y_granite_r0575_H100.jld"
    response_neighbourhood_filename = "neighbours_influence_50y_granite_d20_r165_H100.jld"
    H = 100.;
    results = simulate_ground_hp(response_filename, response_neighbourhood_filename, Tcondin_SH_filename, loadhp_SH_filename, loadhp_DHW_filename, H, n = n )
    # Save the results
    save("results_ground_H100d20.jld", "results", results)
