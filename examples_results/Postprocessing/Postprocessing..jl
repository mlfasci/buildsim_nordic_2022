using JLD

# Air HP
    results_air = load(joinpath(@__DIR__,"examples_results\\results_air.jld"))
    results_air = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_air.jld")["results"]

    el_SH = sum(results_air[1] + results_air[2]) # Wh/year
    el_DHW = sum(results_air[3] + results_air[4]) # Wh/year
    el_tot = el_SH + el_DHW

# Ground HP
    # H = 50, d-> Inf
    results_ground_H50dInf = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H50dInf.jld")["results"]
    el_SH_H50dInf_1 = sum(results_ground_H50dInf[1][1:8760]  .+ results_ground_H50dInf[2][1:8760]) # Wh /  year 1
    el_DHW_H50dInf_1 = sum(results_ground_H50dInf[3][1:8760]  .+ results_ground_H50dInf[4][1:8760]) # Wh / year 1
    el_tot_H50dInf_1 = el_SH_H50dInf_1 + el_DHW_H50dInf_1
    el_SH_H50dInf_25 = sum(results_ground_H50dInf[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50dInf[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50dInf_25 = sum(results_ground_H50dInf[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50dInf[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50dInf_25 = el_SH_H50dInf_25 + el_DHW_H50dInf_25

    # H = 100, d-> Inf
    results_ground_H100dInf = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H100dInf.jld")["results"]
    el_SH_H100dInf_1 = sum(results_ground_H100dInf[1][1:8760]  .+ results_ground_H100dInf[2][1:8760]) # Wh /  year 1
    el_DHW_H100dInf_1 = sum(results_ground_H100dInf[3][1:8760]  .+ results_ground_H100dInf[4][1:8760]) # Wh / year 1
    el_tot_H100dInf_1 = el_SH_H100dInf_1 + el_DHW_H100dInf_1
    el_SH_H100dInf_25 = sum(results_ground_H100dInf[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100dInf[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100dInf_25 = sum(results_ground_H100dInf[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100dInf[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100dInf_25 = el_SH_H100dInf_25 + el_DHW_H100dInf_25

    # H = 50, d = 15
    results_ground_H50d15 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H50d15.jld")["results"]
    el_SH_H50d15_1 = sum(results_ground_H50d15[1][1:8760]  .+ results_ground_H50d15[2][1:8760]) # Wh /  year 1
    el_DHW_H50d15_1 = sum(results_ground_H50d15[3][1:8760]  .+ results_ground_H50d15[4][1:8760]) # Wh / year 1
    el_tot_H50d15_1 = el_SH_H50d15_1 + el_DHW_H50d15_1
    el_SH_H50d15_25 = sum(results_ground_H50d15[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50d15[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50d15_25 = sum(results_ground_H50d15[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50d15[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50d15_25 = el_SH_H50d15_25 + el_DHW_H50d15_25

    # H = 50, d = 20
    results_ground_H50d20 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H50d20.jld")["results"]
    el_SH_H50d20_1 = sum(results_ground_H50d20[1][1:8760]  .+ results_ground_H50d20[2][1:8760]) # Wh /  year 1
    el_DHW_H50d20_1 = sum(results_ground_H50d20[3][1:8760]  .+ results_ground_H50d20[4][1:8760]) # Wh / year 1
    el_tot_H50d20_1 = el_SH_H50d20_1 + el_DHW_H50d20_1
    el_SH_H50d20_25 = sum(results_ground_H50d20[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50d20[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50d20_25 = sum(results_ground_H50d20[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H50d20[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50d20_25 = el_SH_H50d20_25 + el_DHW_H50d20_25

    # H = 100, d = 15
    results_ground_H100d15 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H100d15.jld")["results"]
    el_SH_H100d15_1 = sum(results_ground_H100d15[1][1:8760]  .+ results_ground_H100d15[2][1:8760]) # Wh /  year 1
    el_DHW_H100d15_1 = sum(results_ground_H100d15[3][1:8760]  .+ results_ground_H100d15[4][1:8760]) # Wh / year 1
    el_tot_H100d15_1 = el_SH_H100d15_1 + el_DHW_H100d15_1
    el_SH_H100d15_25 = sum(results_ground_H100d15[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100d15[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100d15_25 = sum(results_ground_H100d15[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100d15[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100d15_25 = el_SH_H100d15_25 + el_DHW_H100d15_25

    # H = 100, d = 20
    results_ground_H100d20 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_ground_H100d20.jld")["results"]
    el_SH_H100d20_1 = sum(results_ground_H100d20[1][1:8760]  .+ results_ground_H100d20[2][1:8760]) # Wh /  year 1
    el_DHW_H100d20_1 = sum(results_ground_H100d20[3][1:8760]  .+ results_ground_H100d20[4][1:8760]) # Wh / year 1
    el_tot_H100d20_1 = el_SH_H100d20_1 + el_DHW_H100d20_1
    el_SH_H100d20_25 = sum(results_ground_H100d20[1][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100d20[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100d20_25 = sum(results_ground_H100d20[3][1 + 24 * 8760 : 25 * 8760]  .+ results_ground_H100d20[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100d20_25 = el_SH_H100d20_25 + el_DHW_H100d20_25

    # Dual HP
    # H = 50, d-> Inf
    results_dual_H50dInf = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H50dInf.jld")["results"]
    el_SH_H50dInf_1 = sum(results_dual_H50dInf[1][1:8760]  .+ results_dual_H50dInf[2][1:8760]) # Wh /  year 1
    el_DHW_H50dInf_1 = sum(results_dual_H50dInf[3][1:8760]  .+ results_dual_H50dInf[4][1:8760]) # Wh / year 1
    el_tot_H50dInf_1 = el_SH_H50dInf_1 + el_DHW_H50dInf_1
    el_SH_H50dInf_25 = sum(results_dual_H50dInf[1][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50dInf[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50dInf_25 = sum(results_dual_H50dInf[3][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50dInf[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50dInf_25 = el_SH_H50dInf_25 + el_DHW_H50dInf_25

    # H = 100, d-> Inf
    results_dual_H100dInf = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H100dInf.jld")["results"]
    el_SH_H100dInf_1 = sum(results_dual_H100dInf[1][1:8760]  .+ results_dual_H100dInf[2][1:8760]) # Wh /  year 1
    el_DHW_H100dInf_1 = sum(results_dual_H100dInf[3][1:8760]  .+ results_dual_H100dInf[4][1:8760]) # Wh / year 1
    el_tot_H100dInf_1 = el_SH_H100dInf_1 + el_DHW_H100dInf_1
    el_SH_H100dInf_25 = sum(results_dual_H100dInf[1][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100dInf[2][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100dInf_25 = sum(results_dual_H100dInf[3][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100dInf[4][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100dInf_25 = el_SH_H100dInf_25 + el_DHW_H100dInf_25

    # H = 50, d = 15
    results_dual_H50d15 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H50d15.jld")["results"]
    el_SH_H50d15_1 = sum(results_dual_H50d15[8][1:8760]  .+ results_dual_H50d15[9][1:8760]) # Wh /  year 1
    el_DHW_H50d15_1 = sum(results_dual_H50d15[15][1:8760]  .+ results_dual_H50d15[16][1:8760]) # Wh / year 1
    el_tot_H50d15_1 = el_SH_H50d15_1 + el_DHW_H50d15_1
    el_SH_H50d15_25 = sum(results_dual_H50d15[8][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50d15[9][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50d15_25 = sum(results_dual_H50d15[15][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50d15[16][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50d15_25 = el_SH_H50d15_25 + el_DHW_H50d15_25

    # H = 50, d = 20
    results_dual_H50d20 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H50d20.jld")["results"]
    el_SH_H50d20_1 = sum(results_dual_H50d20[8][1:8760]  .+ results_dual_H50d20[9][1:8760]) # Wh /  year 1
    el_DHW_H50d20_1 = sum(results_dual_H50d20[15][1:8760]  .+ results_dual_H50d20[16][1:8760]) # Wh / year 1
    el_tot_H50d20_1 = el_SH_H50d20_1 + el_DHW_H50d20_1
    el_SH_H50d20_25 = sum(results_dual_H50d20[8][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50d20[9][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H50d20_25 = sum(results_dual_H50d20[15][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H50d20[16][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H50d20_25 = el_SH_H50d20_25 + el_DHW_H50d20_25

    # H = 100, d = 15
    results_dual_H100d15 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H100d15.jld")["results"]
    el_SH_H100d15_1 = sum(results_dual_H100d15[8][1:8760]  .+ results_dual_H100d15[9][1:8760]) # Wh /  year 1
    el_DHW_H100d15_1 = sum(results_dual_H100d15[15][1:8760]  .+ results_dual_H100d15[16][1:8760]) # Wh / year 1
    el_tot_H100d15_1 = el_SH_H100d15_1 + el_DHW_H100d15_1
    el_SH_H100d15_25 = sum(results_dual_H100d15[8][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100d15[9][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100d15_25 = sum(results_dual_H100d15[15][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100d15[16][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100d15_25 = el_SH_H100d15_25 + el_DHW_H100d15_25

    # H = 100, d = 20
    results_dual_H100d20 = load("C:\\Users\\mlfasci\\OneDrive - KTH\\box_files\\PhD (mlfasci@kth.se 4)\\Project\\Conferences\\BuildSim-Nordic 2022\\buildsim_nordic_2022\\examples_results\\results_dual_H100d20.jld")["results"]
    el_SH_H100d20_1 = sum(results_dual_H100d20[8][1:8760]  .+ results_dual_H100d20[9][1:8760]) # Wh /  year 1
    el_DHW_H100d20_1 = sum(results_dual_H100d20[15][1:8760]  .+ results_dual_H100d20[16][1:8760]) # Wh / year 1
    el_tot_H100d20_1 = el_SH_H100d20_1 + el_DHW_H100d20_1
    el_SH_H100d20_25 = sum(results_dual_H100d20[8][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100d20[9][1 + 24 * 8760 : 25 * 8760]) # Wh /  year 25
    el_DHW_H100d20_25 = sum(results_dual_H100d20[15][1 + 24 * 8760 : 25 * 8760]  .+ results_dual_H100d20[16][1 + 24 * 8760 : 25 * 8760]) # Wh / year 25
    el_tot_H100d20_25 = el_SH_H100d20_25 + el_DHW_H100d20_25
