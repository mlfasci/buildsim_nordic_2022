module buildsim_nordic_2022

    include("simulate_air_hp.jl")
        export simulate_air_hp
    include("simulate_ground_hp.jl")
        export simulate_ground_hp
    include("simulate_dual_hp.jl")
        export simulate_dual_hp

    using HeatPump
    using JLD
    using XLSX

end # module
