function simulate_air_hp(Tair_filename::T, Tcondin_SH_filename::T, loadhp_SH_filename::T, loadhp_DHW_filename::T) where{T<:String}
# Air temperature [K]
    Tair = XLSX.readxlsx(joinpath(@__DIR__,"..","data\\",Tair_filename))["Sheet1"]["H3:H8762"] .+ 273.15;

# Loads and temperatures
    Tcondin_SH = load(joinpath(@__DIR__,"..","data\\",Tcondin_SH_filename))["tr"]
    loadhp_SH = load(joinpath(@__DIR__,"..","data\\",loadhp_SH_filename))["hourload_No_DHW"]
    Tcondin_DHW = fill(50. + 273.15, 8760);
    loadhp_DHW = load(joinpath(@__DIR__,"..","data", loadhp_DHW_filename))["DHW_load"];

# Simulation
    solution_air = air_heatpump(Tair, Tcondin_SH, loadhp_SH, Tcondin_DHW, loadhp_DHW)
end
