function simulate_ground_hp(response_filename::T, Tcondin_SH_filename::T, loadhp_SH_filename::T, loadhp_DHW_filename::T, H::AbstractFloat; n::Int = 1) where {T<:String }
# Ground response
    fls = load(joinpath(@__DIR__,"..","data\\", response_filename))["fls"]

# Loads and temperatures
    Tcondin_SH = load(joinpath(@__DIR__,"..","data\\",Tcondin_SH_filename))["tr"]
    loadhp_SH = load(joinpath(@__DIR__,"..","data\\",loadhp_SH_filename))["hourload_No_DHW"]
    Tcondin_DHW = fill(50. + 273.15, 8760);
    loadhp_DHW = load(joinpath(@__DIR__,"..","data", loadhp_DHW_filename))["DHW_load"];

# Simulation
    solution_ground = groundheatpump(n, fls, H, Tcondin_SH, loadhp_SH, Tcondin_DHW, loadhp_DHW)
end

function simulate_ground_hp(response_filename::T, response_neighbourhood_filename::T, Tcondin_SH_filename::T, loadhp_SH_filename::T, loadhp_DHW_filename::T, H::AbstractFloat; n::Int = 1) where {T<: String}
# Ground response
    fls = load(joinpath(@__DIR__,"..","data\\", response_filename))["fls"]
    flsN = load(joinpath(@__DIR__,"..","data\\", response_neighbourhood_filename))["flsN"]
# Loads and temperatures
    Tcondin_SH = load(joinpath(@__DIR__,"..","data\\","House_Output_tr"))["tr"]
    loadhp_SH = load(joinpath(@__DIR__,"..","data\\","House_Output_Load"))["hourload_No_DHW"]
    Tcondin_DHW = fill(50. + 273.15, 8760);
    loadhp_DHW = load(joinpath(@__DIR__,"..","data\\DHW_load.jld"))["DHW_load"];

# Simulation
    solution_ground = groundheatpump(n, fls, flsN, H, Tcondin_SH, loadhp_SH, Tcondin_DHW, loadhp_DHW)
end
